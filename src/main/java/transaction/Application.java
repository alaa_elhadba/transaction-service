package transaction;


import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.client.Client;

import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import transaction.domain.Transaction;
import transaction.service.TransactionService;


import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static transaction.utils.TransactionTransformers.*;
import static transaction.config.ElasticSearchConfig.*;
import static transaction.utils.JsonUtils.*;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application implements CommandLineRunner {

    @Autowired
    Client client;

    @Autowired
    TransactionService transactionService;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class);
    }

    @Override
    public void run(String... args) throws Exception {
        initElasticSearchIndexAndMapping();
        importData();
    }

    private void initElasticSearchIndexAndMapping() throws IOException, ParseException {
        final IndicesExistsResponse res = client.admin().indices().prepareExists(INDEX).execute().actionGet();
        if (res.isExists()) {
            final DeleteIndexRequestBuilder delIdx = client.admin().indices().prepareDelete(INDEX);
            delIdx.execute().actionGet();
        }
        final CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate(INDEX);
        createIndexRequestBuilder.addMapping(TYPE, getJsonObject("src/main/resources/mapping.json").toString());
        createIndexRequestBuilder.execute().actionGet();
    }

    private void importData() {
        try {
            int i = 1;
            final JSONArray jsonArray = getJsonArray("src/main/resources/transactions.json");
            final Iterator<Map<String, Object>> iterator = jsonArray.iterator();
            while (iterator.hasNext()) {
                final Map<String, Object> transactionMap = iterator.next();
                final Transaction transaction = sourceToTransaction.apply(transactionMap);
                transactionService.putTransaction(String.valueOf(i++), transaction);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
