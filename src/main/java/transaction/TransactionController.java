package transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import transaction.domain.Transaction;
import transaction.domain.TransactionsSum;
import transaction.service.TransactionServiceImpl;

import javax.validation.Valid;

import java.io.IOException;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

@RestController
@RequestMapping("/transactionservice/")
public class TransactionController {

    @Autowired
    TransactionServiceImpl transactionService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "transaction/{id}", method = RequestMethod.GET)
    public Transaction getTransaction(@PathVariable final String id) {
        return transactionService.getTransactionById(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "transaction/{id}", method = RequestMethod.PUT)
    public void putTransaction(@PathVariable final String id, @RequestBody @Valid final Transaction transaction) throws IOException {
        transactionService.putTransaction(id, transaction);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "types/{type}", method = RequestMethod.GET)
    public List<String> getTransactionTypes(@PathVariable final String type) {
        return transactionService.getTransactionIdsByType(type);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "sum/{id}", method = RequestMethod.GET)
    public TransactionsSum getTransactionsSum(@PathVariable final String id) {
        return transactionService.getAmountSumByParentId(id);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleException(Exception e) {
        return e.getMessage();
    }
}
