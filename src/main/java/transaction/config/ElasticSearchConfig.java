package transaction.config;

import org.elasticsearch.client.Client;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.NodeClientFactoryBean;

@Configuration
public class ElasticSearchConfig {

    public static final String INDEX = "transactions";
    public static final String TYPE = "transaction";
    public static final String CLUSTER = "transaction-cluster";

    @Bean
    public Client client() throws Exception {
        NodeClientFactoryBean factory = new NodeClientFactoryBean(true);
        factory.setClusterName(CLUSTER);
        factory.setEnableHttp(true);
        factory.afterPropertiesSet();
        return factory.getObject();
    }
}
