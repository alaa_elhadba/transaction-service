package transaction.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.MoreObjects;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(indexName = "transactions", type = "transaction")
public class EsResult implements Serializable {

    @Id
    @Field
    private String id;
    private Transaction source;

    public EsResult(final String id, final Transaction source) {
        this.id = id;
        this.source = source;
    }

    public EsResult() {
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        final EsResult that = (EsResult) object;
        return Objects.equals(this.id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("source", source)
                .toString();
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    public Transaction getSource() {
        return source;
    }

    public void setSource(Transaction source) {
        this.source = source;
    }
}
