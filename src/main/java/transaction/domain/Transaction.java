package transaction.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.MoreObjects;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Transaction {

    private double amount;
    @NotNull @NotEmpty
    private String type;
    @JsonProperty("parent_id")
    private Long parentId;

    public Transaction() {}

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("amount", amount)
                .add("type", type)
                .add("parent_id", parentId)
                .toString();
    }

    public double getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setAmount(final double amount) {
        this.amount = amount;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setParentId(final Long parentId) {
        this.parentId = parentId;
    }
}
