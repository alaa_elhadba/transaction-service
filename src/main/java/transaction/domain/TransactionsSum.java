package transaction.domain;

public class TransactionsSum {
    private double sum;

    public TransactionsSum(double sum) {
        this.sum = sum;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }
}
