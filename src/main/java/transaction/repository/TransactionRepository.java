package transaction.repository;

import java.io.IOException;
import java.util.List;

import transaction.domain.EsResult;
import transaction.domain.Transaction;

public interface TransactionRepository {

    public Transaction findById(final String id);

    public List<EsResult> findByParentId(final String parentId);

    public List<EsResult> findByType(final String type);

    public void save(final String id, final Transaction transaction) throws IOException;

}
