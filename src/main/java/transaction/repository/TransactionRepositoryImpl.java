package transaction.repository;

import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transaction.domain.EsResult;
import transaction.domain.Transaction;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static transaction.utils.TransactionTransformers.*;
import static transaction.config.ElasticSearchConfig.*;

@Service
public class TransactionRepositoryImpl implements TransactionRepository {

    @Autowired
    private Client client;

    @Override
    public Transaction findById(String id) {
        final Map<String, Object> source = client.prepareGet()
                .setIndex(INDEX)
                .setType(TYPE)
                .setId(id)
                .execute()
                .actionGet()
                .getSource();

        return sourceToTransaction.apply(source);
    }


    @Override
    public List<EsResult> findByParentId(String parentId) {
        final SearchHit[] searchHits =
                getSearchRequestBuilder()
                        .setQuery(boolQuery()
                                .should(termQuery("parent_id", parentId))
                                .should(idsQuery(TYPE).addIds(parentId)))
                        .execute()
                        .actionGet()
                        .getHits()
                        .getHits();

        return searchHitsToEsResults.apply(searchHits);
    }

    @Override
    public List<EsResult> findByType(String type) {
        final SearchHit[] searchHits =
                getSearchRequestBuilder()
                        .setQuery(termQuery("type", type))
                        .execute()
                        .actionGet()
                        .getHits()
                        .getHits();

        return searchHitsToEsResults.apply(searchHits);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void save(final String id, final Transaction transaction) {
        final Map transactionMap = transactionToMap.apply(transaction);
        transactionMap.values().removeAll(Collections.singleton(null));
        getIndexRequestBuilder()
                .setId(id)
                .setSource(transactionMap)
                .execute()
                .actionGet();
    }

    private IndexRequestBuilder getIndexRequestBuilder() {
        return client.prepareIndex()
                .setIndex(INDEX)
                .setType(TYPE);
    }

    private SearchRequestBuilder getSearchRequestBuilder() {
        return client.prepareSearch(INDEX)
                .setTypes(TYPE);
    }
}
