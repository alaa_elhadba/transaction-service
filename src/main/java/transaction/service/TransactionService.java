package transaction.service;


import transaction.domain.Transaction;
import transaction.domain.TransactionsSum;

import java.util.List;

public interface TransactionService {

    List<String> getTransactionIdsByType(String type);

    TransactionsSum getAmountSumByParentId(String id);

    Transaction getTransactionById(String id);

    void putTransaction(final String id, final Transaction transaction);
}
