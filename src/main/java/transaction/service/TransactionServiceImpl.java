package transaction.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transaction.domain.EsResult;
import transaction.domain.Transaction;
import transaction.domain.TransactionsSum;
import transaction.repository.TransactionRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepositoryImpl transactionRepository;

    @Override
    public List<String> getTransactionIdsByType(String type) {
        final List<EsResult> transactions = transactionRepository.findByType(type);
        return transactions
                .stream()
                .map(EsResult::getId)
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public TransactionsSum getAmountSumByParentId(String id) {
        final List<EsResult> transactions = transactionRepository.findByParentId(id);
        final Double sum = transactions
                .stream()
                .mapToDouble(i -> i.getSource().getAmount())
                .sum();
        return new TransactionsSum(sum);
    }

    @Override
    public Transaction getTransactionById(String id) {
        return transactionRepository.findById(id);
    }

    @Override
    public void putTransaction(final String id, final Transaction transaction) {
        transactionRepository.save(id, transaction);
    }

}
