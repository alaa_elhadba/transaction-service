package transaction.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class JsonUtils {

    public static JSONArray getJsonArray(final String path) throws IOException, ParseException {
        final FileReader reader = new FileReader(path);
        final JSONParser jsonParser = new JSONParser();
        final Object obj = jsonParser.parse(reader);
        return (JSONArray) obj;
    }

    public static JSONObject getJsonObject(final String path) throws IOException, ParseException {
        final FileReader reader = new FileReader(path);
        final JSONParser jsonParser = new JSONParser();
        final Object obj = jsonParser.parse(reader);
        return (JSONObject) obj;
    }
}
