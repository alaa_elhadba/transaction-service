package transaction.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.search.SearchHit;
import transaction.domain.EsResult;
import transaction.domain.Transaction;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TransactionTransformers {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    public final static Function<Map<String, Object>, Transaction> sourceToTransaction
            = source -> objectMapper.convertValue(source, Transaction.class);

    public final static Function<Transaction, Map> transactionToMap
            = transaction -> objectMapper.convertValue(transaction, Map.class);

    public final static Function<SearchHit, EsResult> searchHitToEsResult
            = searchHit -> new EsResult(searchHit.getId(), sourceToTransaction.apply(searchHit.getSource()));

    public final static Function<SearchHit[], List<EsResult>> searchHitsToEsResults
            = searchHits -> Arrays.asList(searchHits)
            .stream()
            .map(searchHitToEsResult::apply)
            .collect(Collectors.toList());

}
