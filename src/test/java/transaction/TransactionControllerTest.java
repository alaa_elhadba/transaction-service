package transaction;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import org.elasticsearch.client.Client;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.SpringApplicationConfiguration;

import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import transaction.config.TestConfig;

import transaction.domain.Transaction;
import transaction.domain.TransactionsSum;

import transaction.service.TransactionService;

import java.io.IOException;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestConfig.class)
@WebAppConfiguration
public class TransactionControllerTest {

    private MockMvc mockMvc;

    @Autowired
    TransactionService transactionService;

    @Autowired
    TransactionController transactionController;

    private final static ObjectMapper objectMapper = new ObjectMapper();


    @Before
    public void setup() throws IOException, ParseException {
        mockMvc = MockMvcBuilders.standaloneSetup(transactionController).build();
    }

    @Test
    public void integrationTest() throws Exception {
        final Transaction transaction10 = new Transaction();
        transaction10.setAmount(5000);
        transaction10.setType("cars");
        String transaction10Json = objectMapper.writeValueAsString(transaction10);

        final Transaction transaction11 = new Transaction();
        transaction11.setAmount(10000);
        transaction11.setType("cars");
        transaction11.setParentId(11L);
        String transaction11Json = objectMapper.writeValueAsString(transaction11);

        final TransactionsSum transactionsSum10 = new TransactionsSum(15000);
        final TransactionsSum transactionsSum11 = new TransactionsSum(10000);
        final List<String> types = ImmutableList.of("10");

        mockMvc.perform(put("/transactionservice/transaction/10")
                .contentType(MediaType.APPLICATION_JSON)
                .content(transaction10Json))
                .andExpect(status().isOk());

        mockMvc.perform(get("/transactionservice/transaction/10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(transaction10Json));

        mockMvc.perform(put("/transactionservice/transaction/11")
                .contentType(MediaType.APPLICATION_JSON)
                .content(transaction11Json))
                .andExpect(status().isOk());

        mockMvc.perform(get("/transactionservice/types/cars")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(types)));

        mockMvc.perform(get("/transactionservice/sum/10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(transactionsSum10)));

        mockMvc.perform(get("/transactionservice/sum/11")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(transactionsSum11)));

    }

}
