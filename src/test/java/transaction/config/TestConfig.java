package transaction.config;


import org.elasticsearch.client.Client;
import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.NodeClientFactoryBean;
import org.springframework.jmx.export.annotation.ManagedOperation;
import transaction.TransactionController;
import transaction.repository.TransactionRepository;
import transaction.repository.TransactionRepositoryImpl;
import transaction.service.TransactionService;
import transaction.service.TransactionServiceImpl;

@Configuration
public class TestConfig {

    public static final String CLUSTER = "transaction-cluster";

    @Bean
    public Client client() throws Exception {
        NodeClientFactoryBean factory = new NodeClientFactoryBean(true);
        factory.setClusterName(CLUSTER);
        factory.setEnableHttp(true);
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean
    public TransactionRepository transactionRepository() {
        return new TransactionRepositoryImpl();
    }

    @Bean
    public TransactionController transactionController() {
        return new TransactionController();
    }

    @Bean
    public TransactionService transactionService() {
        return new TransactionServiceImpl();
    }

}
